# TL;DR
**lizzy** is a TUI playlist editor for [_mpv_](https://github.com/mpv-player/mpv).  
Browse or search _instantly_ your music collection by folders and files names. (id3 tags are ignored.)  
You can browse your movies library likewise, if you so wish.  
**lizzy** supports profiles ; and does [little more](#features).  
It is a single python file with no dependency.  


# Screenshot
You'll feel more at home with your collection. And until I come up with a screencast, those shots don't do justice to _lizzy_'s speedy workflow.

<img src="/docs/1.png">

# Installation
### Requirements
No dependencies. (but mpv at run-time to actually play something).  

### Instructions
either **by hand**  
 * download (latest released version of) the script (only lizzy file is needed)  
 * place lizzy somewhere along your `$PATH` (`~/.local/bin/` in my case)  
 * make it executable (`chmod +x lizzy`)  

or **pip (master branch)**  
``` shell
pip install --user --upgrade https://gitlab.com/Arnaudv6/lizzy/-/archive/master/lizzy-master.zip
# if your system provides a pip3 command, use it instead
```

or **curl (master branch)**  
``` shell
curl -o ~/.local/bin/lizzy https://gitlab.com/Arnaudv6/lizzy/-/raw/master/lizzy ; chmod +x ~/.local/bin/lizzy
```

or **wget (master branch)**  
``` shell
wget -q -c -P ~/.local/bin https://gitlab.com/Arnaudv6/lizzy/-/raw/master/lizzy ; chmod +x ~/.local/bin/lizzy
```


# Usage
 - run `lizzy`
 - if you run lizzy for the first time, press `[ctrl]+[u]` to index your collection
 - for help, from the command-line: `lizzy --help`
 - consider adding `ESC quit` to `~/.config/mpv/input.conf`
 - optionally, you might want to define profiles, in `~/.config/lizzy/config.ini` like so:
    ```ini
    [DEFAULT]
    # values here get applied by default to
    # any section not defining a setting.
    userMusicsPath = /home/foo/music
    backEndCommand = mpv --no-video --term-osd-bar
    printWaitDelay = 1
    # printWaitDelay is int: seconds of pause after a warning message

    [Second user]
    userMusicsPath = /home/foo/music
    skipExtensions = FLAC mp3
    # skipExtensions: case insensitive, space separated, no dot.
    # this will override default list (jpg jpeg png txt nfo ini db srt)

    # want to find movies also?
    [films]
    userMusicsPath = ~/Video Films
    backEndCommand = mpv
    # consider adding following line to '~/.bashrc':
    # alias films='lizzy --profile=films'
    ```


# Key bindings
Those are in-app bindings.  

| binding <br> `^A`: combine [ctrl]+[a] | action |
| :---: | ------ |
| [tab] | go from search-terms to results, then to playlist |
| [space] | add selected item to playlist's end |
| [enter] | play |
| ^A | in left pane, add all results to playlist <br> in right pane, clear playlist |
| ^F | focus top search field |
| ^U | update current profile's library data base |
| ^Q | ask for confirmation and quit |
| ^R | (de-)activate shuffle-mode for playback |
| ^S | save current playlist in /tmp directory as .m3u |
| ^Z | undo/redo last playlist edit |

You can control lizzy at play-time with standard tools as it leverages plain mpv:  
you could give [MPRIS D-Bus](https://github.com/hoyon/mpv-mpris) interfaces a try, or turn to your prefered solution.


# The vision
 - Single file, no dependency, only python (3.6)
 - Simple & fast workflow: do naught but feeding mpv with a playlist
 - Ignore id-tags, track durations... search for matches only in files paths and names
 - Minimal disk access (no startup reindexing, don't require mpv to scrap disk either...)
 - Never modify your music files, lizzy writes only in `~/.config/lizzy/` or `/tmp/`
 - No online services integration but webradios and mpv-supported URLs: either directly typed URLs or as *m3u* files. 
 - No deamon-client architecture, no modules, no interraction through system-bus or from the outside.  
     At play time you can interract with mpv anyway you like though.  
 - Logical, agnostic [key bindings](#key-bindings) (no _emacs / vim_, see [related projects](#related-projects))
 - No complex keyboard workflow either (multiple items selection, cut/paste/move in playlist...)  
     Better force a coherent/simple workflow than try accomodating everyone.  
     But an item can still be copied to the end of the playlist hitting `[space]`  
 - For the time being, not customisable key bindings (open a bug to propose a binding if you feel like to)

Refer to [RATIONALE.md](/RATIONALE.md) for more technical details


# Features
 - play your complete collection in random order
 - profiles support so you can have several collections and associated playlists (i.e. use _lizzy_ as music / movies player)
 - paste radio stream URLs, drag files from your computer to _lizzy_
 - use existant _M3U_ file as a playlist


# FAQ

### Why at all?
My Music library is well organized (folders and files well named). This is a opinionated player to match it.  
It is also (remotely) inspired by _[pogo](https://github.com/jendrikseipp/pogo)_ and comes as a replacement of _[m](https://gitlab.com/Arnaudv6/m)_.  
I complexified a tad the workflow over that of _[m](https://gitlab.com/Arnaudv6/m)_ so as to gain two features (plus all the advantages that come along):  
 1. retaining the playlist for next (or future) session.
 2. a quick browse mode, for when I don't know at once what I want to listen to.  

### Why the name?
As a tribute to my beloved wife.

### How does lizzy perform on slow(TM) hardware?
  I test lizzy on Pi zero with Raspberry Pi OS through ssh, with a ~20.000 tracks library and it does very well!  

  -  screen rendering is snappy: never an issue performance-wise.  
  -  filtering the collection is fast enough that I can't feel differences on small implementation details (on above slow setup).  
  -  still, any feedback/commit is welcome!  

### Can lizzy work over network like mpd, which plays remote music on remote server?
  this is not the typical use for lizzy. But yes. In fact lizzy delegates this also:  

  - you can play music on remote server by running lizzy there, through ssh.  
  - lizzy can (locally or remotely) play remote music through sshfs for instance.

### Is lizzy safe to use?
  lizzy is no security concern, but neither is it an added security barrier on top of mpv. (See [this warning](https://mpv.io/manual/master/#options-playlist))  
  lizzy does not check what files you selected to hand-in to mpv.  
  So general prudence applies as anywhere. But you likely own the collection you are using??  
  Just beware outlandish playlists-files (.m3u): an ill-intended person might populate one with URLs to wild things...

### Terminals compatibility

  - Everything worked in FreeBSD when tried in June '20.  
  - Some terminals allow different emulation types, and default selected one might depend on your system.  
     For instance in Qterminal (often defaulting to `vt420pc`), setting emulation to `linux` gives a consistent behavior.
  - LilyTerm is super buggy and [has seen no release since 2013](https://github.com/Tetralet/LilyTerm/issues/116). I won't support it.
  - Asian oriented terminals I can't test, sorry, any help welcome.

### I can edit my playlist, but I can't play, please help me!
  - Does mpv play any sound when used outside of lizzy?  
     mpv might print nice suggestions about pulseaudio/alsa/oss not running or else.

### Related projects
  - https://github.com/mcsinyx/comp  
  - https://github.com/ryanflannery/vitunes (or vitunes.org)  
  - https://pypi.org/project/mps/
  - https://github.com/lwilletts/mpvc


